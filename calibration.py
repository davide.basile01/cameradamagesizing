import glob
import cv2
import numpy as np

# Define the chess board rows and columns
rows = 6
cols = 9
# Set the termination criteria for the corner sub-pixel algorithm
criteria = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 30, 0.001)

# Prepare the object points: (0,0,0), (1,0,0), (2,0,0), ..., (6,5,0). They are the same for all images
objectPoints = np.zeros((rows * cols, 3), np.float32)
objectPoints[:, :2] = np.mgrid[0:rows, 0:cols].T.reshape(-1, 2)

# Create the arrays to store the object points and the image points
objectPointsArray = []
imgPointsArray = []

print("loading images")

# set different distance

new1 = "./new1/*.jpg"
fullp = "./fullsizeportrait/*.jpg"
full = "./fullsize/*.jpg"
met = "./4.62m/*.jpg"
full4m_26_11 = "./acq261118/4M/*.jpg"
full4mr_26_11 = "./acq261118/4Mruotate/*.jpg"
full2m_26_11 = "./acq261118/2Mbis/*.jpg"
full1m_26_11 = "./acq261118/1m/*.jpg"
honor1m = "./honor1m/*.jpg"
honor2m = "./honor2m/*.jpg"
honor5m = "./honor5m/*.jpg"
vecchie1m = "./1m/*.jpg"


k = 1

for path in glob.glob(vecchie1m):

    print("image " + k.__str__() + " loaded")

    k = k + 1

    # Load the image and convert it to gray scale
    img = cv2.imread(path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (rows, cols), None)

    # Make sure the chess board pattern was found in the image
    if ret:
        print("Found")

        # Refine the corner position
        corners = cv2.cornerSubPix(gray, corners, (5, 5), (-1, -1), criteria)

        # Add the object points and the image points to the arrays
        objectPointsArray.append(objectPoints)
        imgPointsArray.append(corners)

        # Draw the corners on the image
        cv2.drawChessboardCorners(img, (rows, cols), corners, ret)

    # Display the image
    #
    # cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('image', 768, 1366)
    # cv2.imshow('image', img)
    # while 1:
    #     k = cv2.waitKey(33)
    #     if k == 13:  # Enter key to exit
    #         break

# Calibrate the camera and save the results
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objectPointsArray, imgPointsArray, gray.shape[::-1], None, None)
print(dist)
np.savez("./vecchie1m" + ".npz", mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)

# Print the camera calibration error
error = 0

for i in range(len(objectPointsArray)):
    imgPoints, _ = cv2.projectPoints(objectPointsArray[i], rvecs[i], tvecs[i], mtx, dist)
    error += cv2.norm(imgPointsArray[i], imgPoints, cv2.NORM_L2) / len(imgPoints)

print("Total error: ", error / len(objectPointsArray))

cv2.destroyAllWindows()
