
file = imread('C:\Users\david\workspace\cameraDamageSizing\main\nexus5xportrait1mfront\test1.jpg');

% Display one of the calibration images
magnification = 25;

imOrig = file;
figure; imshow(imOrig, 'InitialMagnification', magnification);
title('Input Image');
squareSize = 50;


[im, newOrigin] = undistortImage(imOrig, cameraParams, 'OutputView', 'full');

imagePoints1 = [1104, 958;
                1200, 958;
                1200, 1054;
                1103, 1054];
            
normax = norm(imagePoints1(2, :) - imagePoints1(1, :));
normay = norm(imagePoints1(3, :) - imagePoints1(2, :));

% Compute the diameter of the coin in millimeters.
dX = norm(imagePoints1(2, :) - imagePoints1(1, :)) / (cameraParams.IntrinsicMatrix(1, 1)) * 1000;
dY = norm(imagePoints1(3, :) - imagePoints1(2, :)) / (cameraParams.IntrinsicMatrix(2, 2)) * 1000;
diameterInMillimeters = hypot(dX, dY);
area = dX * dY;
figure; 
hold on;
imshow(im, 'InitialMagnification', magnification);
rectangle('Position', [imagePoints1(1,1), imagePoints1(1,2), normax, normay], 'EdgeColor', 'Y', 'LineWidth' ,1);
text(1150, 940, 'ludovico gay');
% imwrite(im, 'undistortedImageTest.jpg', 'jpg');
title('Detected box');
fprintf('Measured diagonal = %0.2f mm\n', diameterInMillimeters);
fprintf('Measured area = %0.2f mm\n', area);
fprintf('Measured side = %0.2f mm\n', dX);
fprintf('Measured side = %0.2f mm\n', dY);



